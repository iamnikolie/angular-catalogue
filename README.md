# README #

Простой каталог, использующий jQuery, Twitter Bootstrap, AngularJS.

### Что принимает? ###

Принимает JSON формата 

```
#!php

        [
            'image' => 'string',
            'id' => 'integer',
            'name' => 'string',
            'description' => 'text',
            'price' => 'integer',
            'currency' => 'string',
        ]
```

Для генерации JSON из PHP используем файл tojson.php.

Соответствующий вывод в ng-repeat модуле на странице. Можно передавать любой набор параметров и по-любому их выводить.

### Что отдает? ###

В случае, если пользователь заполнит оба поля - имя и фамилия - отправляет ресурс с полями: имя, фамилия, массив ID выбранных продуктов

catch.php "ловит" этот запрос и возвращает его обратно - dev mode on)

Пробуй  [http://imn.co.ua/angular-catalogue/](http://imn.co.ua/angular-catalogue/)