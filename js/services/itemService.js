
angular.module('itemService', [])
    .factory('Item', function($http) {
        return {
            get : function() {
                return $http.get('data/test.json');
            },
            save : function(commentData) {
                return $http.put('catch.php', $.param(commentData));
            },

        }
    });