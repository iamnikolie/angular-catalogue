
function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

var itemApp = angular.module('itemApp', ['mainCtrl', 'itemService']);

$(document).ready(function(){
    var width = $('img').width();
    console.log(width);
    $('img').height(width);
});