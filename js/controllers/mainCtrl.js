
angular.module('mainCtrl', [])

    .controller('mainController', function($scope, $http, Item) {

        $scope.sessionId = guid();

        $scope.cart = [];
        $scope.itemData = {};
        $scope.errors = {};
        $scope.countItems = 0;

        Item.get()
            .success(function(data) {
                $scope.items = data;
            });

        $scope.addToCart = function (id, count) {
            var assigned = {};
            if ($scope.errors.message != '') {
                delete $scope.errors.message;
            }
            $scope.cart.forEach(function(item, i){
                if (item.id == id) {
                    assigned.marker = true;
                    assigned.id = i;
                }
            });
            if (assigned.marker) {
                $scope.cart[assigned.id].count =  parseInt($scope.cart[assigned.id].count) + parseInt(count);
            } else {
                var added = {
                    'id':  parseInt(id),
                    'count': parseInt(count)
                };

                $scope.cart.push(added);
                $scope.countItems = parseInt($scope.countItems) + 1;
            }

            var transaction = {

                'id': parseInt(id),
                'fname': $scope.fname,
                'lname': $scope.lname,
                'count': parseInt(count),
                'timestamp': new Date(),
                'guid': guid(),
                'sessionid': $scope.sessionId,
                'action': 'add'

            };
            $http.put('some/url/to/', transaction);
            console.log('transaction', transaction);

            console.log($scope.cart);
        };

        $scope.loginFinish = function () {

            var transaction = {

                'fname': $scope.fname,
                'lname': $scope.lname,
                'timestamp': new Date(),
                'guid': guid(),
                'sessionid': $scope.sessionId,
                'action': 'login'

            };
            $http.put('some/url/to/', transaction);
            console.log('transaction', transaction);
        };

        $scope.deleteFromCart = function (id, count) {
            var assigned  = {};
            $scope.cart.forEach(function(item, i){
                if (item.id == id) {
                    assigned.marker = true;
                    assigned.id = i;
                }
            });
            if (assigned.marker) {
                var counted = parseInt($scope.cart[assigned.id].count) - parseInt(count);

                $scope.cart[assigned.id].count = (counted > 0) ? counted : 0;
                if (counted < 0) {
                    $scope.errorCountMessage = "You want to delete too much items!";
                    $("#error-count").modal('toggle');
                    delete $scope.cart[assigned.id];
                    $scope.countItems = (parseInt($scope.countItems) - 1 > 0) ? parseInt($scope.countItems) - 1 : 0;
                    console.log($scope.countItems);
                }
            } else {
                $scope.errorCountMessage = "You want to delete unsigned items";
                $("#error-count").modal('toggle');
            }

            var transaction = {

                'id': parseInt(id),
                'fname': $scope.fname,
                'lname': $scope.lname,
                'count': parseInt(count),
                'timestamp': new Date(),
                'guid': guid(),
                'sessionid': $scope.sessionId,
                'action': 'delete'

            };
            $http.put('some/url/to/delete', transaction);
            console.log('transaction', transaction);

            console.log($scope.cart);
        };

        $scope.pushCart = function () {
            $('#send-cart').modal('toggle');
        };

        $scope.sendAllData = function () {
            if ($scope.cart.length < 1){
                $scope.errors.message = 'Your cart is empty!';
            } else {
                if ($scope.fname && $scope.lname) {

                    $scope.itemData.fname = $scope.fname;
                    $scope.itemData.lname = $scope.lname;

                }
                $scope.itemData.sessionid = $scope.sessionId;
                $scope.itemData.arrayOfItems = $scope.cart;
                console.log($scope.itemData);
                Item.save($scope.itemData)
                    .success(function(data){
                        console.log(data);
                        $('#send-cart').modal('toggle');
                        $("#result-modal").modal('toggle');

                        $scope.cart = [];
                        $scope.itemData = {};
                        $scope.errors = {};
                    });
            }
        };

    });