<?php


    $array = [

        1 => [
            'image' => 'images/1.jpg',
            'id' => '1',
            'name' => 'Some Test Name',
            'description' => 'Some default description',
            'price' => '300',
            'currency' => 'EUR',
        ],

        2 => [
            'image' => 'images/2.jpg',
            'id' => '2',
            'name' => 'Another Test Name',
            'description' => 'Some another default description',
            'price' => '300',
            'currency' => 'USD',
        ],

    ];

    $content = json_encode($array);
    file_put_contents('data/test.json', $content);